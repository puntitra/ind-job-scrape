#!/usr/bin/env python
"""
Filename: ind-job-scrape.py
Description: Given job and location, scrape job advertisements.
usage: ind-job-scrape.py [-h] --job JOB --location LOCATION --country COUNTRY
                         [--debug] [--enable-console-log]
                         [--log-level [{CRITICAL,FATAL,ERROR,WARN,WARNING,INFO,
                          DEBUG,NOTSET}]]
Output: *.csv file containing job posts with the following fields:
SCHEMA = [
    'url',
    'title',
    'employer',
    'location',
    'country',
    'posted_date',
    'summary',
    'description'
    ]
"""
import argparse
import logging
import requests
import json
import sys
import re
from datetime import datetime, timedelta
from os import devnull, linesep
from urllib.parse import urlparse
from pathlib import Path
from html import unescape
from bs4 import BeautifulSoup as bs
import pandas as pd
import iso3166

from urllib3.connectionpool import log as urllib_logger
from selenium.webdriver.remote.remote_connection import LOGGER as selenium_logger
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote import remote_connection
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import NoSuchElementException, TimeoutException


SCHEMA = [
    'url',
    'title',
    'employer',
    'location',
    'country',
    'posted_date',
    'summary',
    'description'
    ]


logger = logging.getLogger(__name__)


def setup_log(console=False, log_level='INFO', server_log_level='ERROR'):
    """
    Setup logging.

    Arguments:
    console -- display logging output to streams (default False)
    log_level -- log level (default 'INFO')
    server_log_level -- log level for 3rd parties (default 'ERROR')
    """
    log_format = '[%(asctime)s] %(levelname)s :: %(module)s.%(funcName)s :'
    log_format += ': %(message)s'
    date_format = '%m/%d/%y %H:%M:%S'
    log_file = '{}.log'.format(Path(__file__).stem)
    log_handlers = [logging.FileHandler(log_file)]
    log_handlers.append(logging.StreamHandler()) if console else log_handlers

    logger = logging.basicConfig(level=log_level, handlers=log_handlers,
                                 format=log_format, datefmt=date_format)

    selenium_logger.setLevel(server_log_level) # Selenium WebDriver logger
    urllib_logger.setLevel(server_log_level)   # Urllib3 logger imported by 
                                               # Selenium
    if logging.DEBUG: list_loggers()


def list_loggers():
    """List all loggers being used including the root logger."""
    loggers = [logging.getLogger(name)
               for name in logging.root.manager.loggerDict]
    loggers.insert(0, logging.getLogger())
    for logger in map(str, loggers):
        logging.debug(logger)


def get_arguments():
    """Return command line arguments passed in."""
    arg_parser = argparse.ArgumentParser()
    # >>> Required arguments >>>
    arg_parser.add_argument('--job', type=str, help='job position',
                            required=True)
    arg_parser.add_argument('--location', type=str, help='job location',
                            required=True)
    arg_parser.add_argument('--country', type=str, help='job country',
                            required=True)
    # <<< Required arguments <<< 
    # >>> Optional arguments >>>
    arg_parser.add_argument('--debug', dest='log_level', action='store_const',
                            const='DEBUG', default='INFO',
                            help='enable debug mode')
    arg_parser.add_argument('--log-level', dest='log_level', default='INFO',
                            choices=list(logging._nameToLevel.keys()),
                            nargs='?', const='INFO', type=str.upper,
                            help='set log level')
    arg_parser.add_argument('--enable-console-log', dest='console',
                            action='store_true', help='enable console log')
    # <<< Optional arguments <<<
    args = arg_parser.parse_args()

    return {
        'job': args.job, 'location': args.location, 'country': args.country,
        'log_level': args.log_level, 'console': args.console,
        }


def get_base_url(country):
    """Return Indeed base URL from a given country name."""
    cname = [c for c in iso3166.countries_by_name.keys()
             if country.lower() in c.lower()][0]
    url = None

    if cname is not None:
        ccode = iso3166.countries[cname].alpha2.lower()
        if ccode == 'us':
            url = 'https://indeed.com'
        else:
            url = 'https://{}.indeed.com'.format(ccode)

    return url


def does_url_exist(url):
    """Check if URL exists."""
    try:
        req = requests.get(url)
        code = req.status_code
        return (True, code) if code == 200 else (False, code)
    except:
        return (False, None)


def get_search_url(base_url, job, location, post_age=21, english_only=True):
    """Return search URL."""
    search_url = '{}/Jobs?q={}&l={}&fromage={}'.format(base_url,
                                                       '+'.join(job.split()),
                                                       '+'.join(location.split()),
                                                       post_age)
    return '{}&lang=en'.format(search_url) if english_only else search_url


def get_driver(browser='firefox'):
    """
    Return a webdriver instance for specified browser and option(s).

    Argument:
    browser -- Web browser (default 'firefox')

    Return:
    driver -- A WebDriver instance
    """
    driver_dict = {
        'firefox': {'options': webdriver.FirefoxOptions,
                    'driver': webdriver.Firefox},
        # 'chrome': {'options': webdriver.ChromeOptions, 
        #            'driver': webdriver.Chrome},
        }

    browser = browser.lower()
    if browser not in driver_dict.keys(): browser = 'firefox'

    options = driver_dict[browser]['options']()
    options.headless = True
    # >>> Firefox specifics >>>
    options.set_preference('dom.webnotifications.enabled', False)
    caps = DesiredCapabilities().FIREFOX
    caps['pageLoadStrategy'] = 'eager'
    # <<< Firefox specifics <<<
    driver = driver_dict[browser]['driver'](capabilities=caps,
                                            options=options,
                                            service_log_path=devnull)
    return driver


def search(driver, url, job, location, timeout=5):
    """Enter search URL and check search results."""
    logger.info('Searching...')
    try:
        driver.get(get_search_url(url, job, location))
        wait = WebDriverWait(driver, timeout)
        wait.until(ec.presence_of_element_located((By.ID, "resultsBody")))

    except TimeoutException:
        logger.error("Failed to load result page. Try again.")
        return False

    else:
        try:
            driver.find_element_by_id('searchCount')
        except NoSuchElementException:
            logger.info('0 results. Try gain with different job/location.')
            return False
        else:
            return True


def dismiss_alert(driver, timeout=5):
    """If alert is present, dismiss it."""
    try:
        logger.info('Handling an alert...')
        wait = WebDriverWait(driver, timeout)
        wait.until(ec.alert_is_present(), 'Waiting for alert timed out...')

    except TimeoutException:
        logger.debug('No alert is present.')

    else:
        driver.switch_to.alert.dismiss()
        logger.debug('Alert dismissed.')


def get_posted_date(date_text):
    days = int(''.join(filter(str.isdigit, date_text)) or 0)
    posted_date = datetime.now() - timedelta(days=days)
    return posted_date.strftime('%m/%d/%y')


def scrape_job_description(driver, job_post, timeout=5):
    desc_element_id = 'vjs-content'
    try:
        driver.execute_script('arguments[0].click();', job_post)
    except ElementClickInterceptedException as e:
        dismiss_alert(driver, timeout=5)
        driver.execute_script('arguments[0].click();', job_post)
    else:
        try:
            wait = WebDriverWait(driver, timeout)
            wait.until(ec.presence_of_element_located((By.ID, desc_element_id)))
        except TimeoutException:
            logger.error('Failed to load job description.')
            return None
        else:
            return driver.find_element_by_id(desc_element_id).text


def scrape_job_post(driver, job_post, timeout=5):
    job_data = [None]*7
    try:
        url = job_post.find_element_by_class_name('jobtitle') \
                      .get_attribute('href')
        title = job_post.find_element_by_class_name('jobtitle').text
        employer = job_post.find_element_by_class_name('company').text
        location = job_post.find_element_by_class_name('location').text
        posted_date = get_posted_date(job_post.find_element_by_class_name('date').text)
        summary = job_post.find_element_by_class_name('summary').text
        job_data[:-1] = (url, title, employer, location, posted_date, summary)
        job_data[-1] = scrape_job_description(driver, job_post, timeout=timeout)

    except Exception as e:
        msg = 'Failed to locate elements in job post.'
        log_exception(driver, e, error_msg=msg, tear_down=False)

    else:
        logger.debug('Successfully scraped {}, {}, {}'.format(title, employer))

    finally:
        return job_data


def has_next_page(driver):
    try:
        btn_next = driver.find_element_by_class_name('np')
    except NoSuchElementException:
        return False
    else:
        return True


def load_next_page(driver, url, timeout=5):
    """manipulate start parameter in the URL."""
    at_url = driver.current_url
    wait = WebDriverWait(driver, timeout)
    try:
        driver.get(url)
        wait.until(ec.url_changes(at_url))
    except TimeoutException as e:
        log_exception(driver, e, error_msg='Failed to load next result page.')
    else:
        try:
            wait.until(ec.visibility_of_element_located((By.ID, 'pageContent')))
        except TimeoutException:
            logger.error('Failed to load job cards.')
        else:
            logger.debug('Successfully loaded {}'.format(driver.current_url))


def write_csv(jobs, filename, schema):
    logger.debug('Writing {} jobs to {}...'.format(len(jobs), filename))
    df = pd.DataFrame(jobs, columns=schema)
    with open(filename, 'a') as f:
        df.to_csv(f, index=False, header=f.tell()==0)


def url2country(url):
    """Return country name extracted from indeed URL."""
    logger.debug('Extracting country from {}'.format(url))
    domain = urlparse(url).netloc
    token = domain.split('.')[0]
    ccode = token if token != 'indeed' else 'us'
    return iso3166.countries.get(ccode).name


def scrape_job_posts(driver, schema, batch_size=30, timeout=5,
                     output_prefix='indeed_jobs'):
    search_url = driver.current_url
    country = url2country(search_url)
    timestamp = datetime.now().strftime('%d%m%y_%H%M')
    filename = '{}_{}.csv'.format(output_prefix, timestamp)
    start = 0
    cont = True
    job_batch = []
    job_set = set()
    logger.info('Scraping job posts on {}'.format(driver.current_url))

    # may wait for a condition here
    while cont:
        try:
            job_posts = driver.find_elements_by_class_name('jobsearch-SerpJobCard')
        except NoSuchElementException as e:
            msg = 'Failed to locate job posts. URL: {}'.format(driver.current_url)
            log_exception(driver, e, error_msg=msg)

        else:
            for job_post in job_posts:
                job = scrape_job_post(driver, job_post, timeout=timeout)
                if job[0] not in job_set:   # Only add unique job posts
                    job.insert(4, country)
                    job_set.add(job[0])
                    job_batch.append(job)
                if (len(job_batch) == batch_size):
                    write_csv(job_batch, filename, schema)
                    job_batch.clear()

        if has_next_page(driver):
            start += 10
            url = '{}&start={}'.format(search_url, start)
            load_next_page(driver, url)
        else:
            cont = False
            if len(job_batch) > 0:
                write_csv(job_batch, filename, schema)
                job_batch.clear()

    logger.info('{} job posts have been written to {}'.format(len(job_set),
                                                              filename))


def make_output_prefix(*args):
    """
    Transform string(s) passed in by removing punctuations, lowercasing, and
    joining them with underscores.
    """
    tokens = filter(None, re.split('[, \-()_\"\']+', ' '.join(args)))
    return '_'.join(tokens).lower()


def terminate(driver, exit_code=0):
    """Quit WebDriver session (dispose and terminate) and exit the script."""
    if driver is not None:
        driver.quit()
    sys.exit(exit_code)


def log_exception(driver, e, error_msg=None, tear_down=True):
    """Display exception/error log messages."""
    error_msg = '' if error_msg is None else error_msg
    debug_msg = e.message if hasattr(e, 'message') else str(e)

    logger.error(error_msg)
    logger.debug(debug_msg)

    if tear_down:
        terminate(driver, exit_code=1)


class MultipleExpectedConditions:
    """
    This is a custom expected condition class. Use this class to check
    multiple conditions with WebDriverWait.until
    Source: https://selenium-python.readthedocs.io/waits.html

    Example:
    cons = [
        ec.element_to_be_clickable((By.ID, 'mybutton')),
        ec.element_to_be_clickable((By.ID, 'mylink'))
        ]
    WebDriverWait(driver, 5).until(MultipleExpectedConditions(cons))
    """
    def __init__(self, expected_conditions):
        self.expected_conditions = expected_conditions

    def __call__(self, driver):
        for ec in self.expected_conditions:
            if not ec(driver):
                return False
            return True


def get_argument_log(args):
    """Return a string with all command line arguments passed in."""
    arg_list =[]
    for k,v in args.items():
        new_v = '*'*len(v) if k == 'password' else v
        arg_list.append('{}: {}'.format(k, new_v))
    return ', '.join(arg_list)


def main():
    args = get_arguments()
    setup_log(console=args['console'], log_level=args['log_level'])
    logger.debug('Arguments: {}'.format(get_argument_log(args)))

    driver = get_driver()
    has_result = search(driver, get_base_url(args['country']),
                        args['job'], args['location'])
    if has_result:
        prefix = make_output_prefix(args['job'],
                                    args['location'],
                                    args['country'])
        scrape_job_posts(driver, SCHEMA,output_prefix=prefix)

    terminate(driver)


if __name__ == '__main__':
    main()
