[TOC]

# Indeed Job Post Scraper

This repository offers a Python script and its prototype (in .ipynb format) to scrape job posts including job descriptions on Indeed based on position and location.

## Usage

```bash
ind-job-scrape.py [-h] --job JOB --location LOCATION --country COUNTRY [--debug] [--enable-console-log] --log-level [{CRITICAL,FATAL,ERROR,WARN,WARNING,INFO,DEBUG,NOTSET}]]
```

## Example

```bash
python3 ind-job-scrape.py --job bartender --location Berlin --country Germany --enable-console-log
```

## Schema

The output is in \*.csv format (i.e. bartender_berlin_germany_040121_1125.csv). The schema is listed below.

- Job URL 
- Job title
- Employer
- Location
- Country
- Posted date
- Job summary
- Job description

## Libraries
The main libraries used for this repository are selenium, beautifulsoup4, requests, and pandas.

## Setup

There are two parts, ind-job-scrape and Selenium driver.

### ind-job-scrape:

Clone this repository.
```bash
git clone https://puntitra@bitbucket.org/puntitra/ind-job-scrape.git
```

Change working directory to ind-job-scrape.
```bash
cd ind-job-scrape
```

Install packages. *Note:* Python3 is required. Also, using virtual environment is highly recommended.
```bash
python3 -m pip install requirements.txt
```

If you want to be able to run the prototype, `ind-job-scrape.ipynb`, you will need to install additional packages. This is optional, the notebook's static HTML file is included.
```bash
python3 -m pip install requirements_jl.txt
```

### Selenium Driver (>=0.29):

Follow the instructions for Firefox (geckodriver) in https://selenium-python.readthedocs.io/installation.html#drivers

In short, download geckdriver and extract it to one of the paths in an environment variable, PATH. For example, env/bin, /usr/bin, or /usr/local/bin. Alternatively, you could extract it to any directory and add that directory to your PATH.

